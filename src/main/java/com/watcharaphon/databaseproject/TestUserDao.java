/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.watcharaphon.databaseproject;

import com.watcharaphon.databaseproject.dao.UserDao;
import com.watcharaphon.databaseproject.helper.DatabaseHelper;
import com.watcharaphon.databaseproject.model.User;

/**
 *
 * @author Lenovo
 */
public class TestUserDao {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for(User u: userDao.getAll()) {
            System.out.println(u);
        }
        System.out.println("");
//        User user1 = userDao.get(2);
//        System.out.println(user1);
        
//        User newUser = new User("user3", "password", 2, "F");
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);
//        insertedUser.setGender("M");
//        user1.setGender("F");
//        userDao.update(user1);
//        User updateUser = userDao.get(user1.getId());
//        System.out.println(updateUser);
        
//       System.out.println("");
        
//        userDao.delete(user1);
//        System.out.println(userDao.getAll());
//        for(User u: userDao.getAll()) {
//            System.out.println(u);
//        }
        
//        System.out.println("");
        
        for(User u: userDao.getAll(" user_name like 'u%' ", " user_name asc, user_gender desc ")) {
            System.out.println(u);
        }
        
        DatabaseHelper.Close();
    }
    
}
